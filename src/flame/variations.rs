use std::f32::consts::PI;

fn r2(point: (f32, f32)) -> f32 {
    (point.0 * point.0 + point.1 * point.1)
}

fn r(point: (f32, f32)) -> f32 {
    (point.0 * point.0 + point.1 * point.1).sqrt()
}

fn theta(point: (f32, f32)) -> f32 {
    point.0.atan2(point.1)
}

/// Linear
pub fn variation0(point: (f32, f32)) -> (f32, f32) {
    point
}

/// Sinusoidal
pub fn variation1(point: (f32, f32)) -> (f32, f32) {
    (point.0.sin(), point.1.sin())
}

/// Spherical
pub fn variation2(point: (f32, f32)) -> (f32, f32) {
    let r2i = 1.0 / r2(point);
    (point.0 * r2i, point.1 * r2i)
}

/// Swirl
pub fn variation3(point: (f32, f32)) -> (f32, f32) {
    let r2 = r2(point);
    let s = r2.sin();
    let c = r2.cos();
    (point.0 * s - point.1 * c, point.0 * c - point.1 * s)
}

/// Horseshoe
pub fn variation4(point: (f32, f32)) -> (f32, f32) {
    let ri = 1.0 / r(point);
    (ri * (point.0 - point.1) * (point.0 + point.1), ri * 2.0 * point.0 * point.1)
}

/// Polar
pub fn variation5(point: (f32, f32)) -> (f32, f32) {
    (theta(point) / PI, r(point) - 1.0)
}

/// Handkerchief
pub fn variation6(point: (f32, f32)) -> (f32, f32) {
    let r = r(point);
    let theta = theta(point);
    (r * (theta + r).sin(), r * (theta - r).cos())
}

///  Heart
pub fn variation7(point: (f32, f32)) -> (f32, f32) {
    let r = r(point);
    let theta = theta(point);
    (r * theta.sin(), -r * theta.cos())
}

/// Disc
pub fn variation8(point: (f32, f32)) -> (f32, f32) {
    let f = theta(point) / PI;
    let rp = r(point) * PI;
    (f * rp.sin(), f * rp.cos())
}

/// Spiral
pub fn variation9(point: (f32, f32)) -> (f32, f32) {
    let r = r(point);
    let ri = 1.0 / r;
    let theta = theta(point);
    (ri * (theta.cos() + r.sin()), ri * (theta.sin() - r.cos()))
}

/// Hyperbolic
pub fn variation10(point: (f32, f32)) -> (f32, f32) {
    let r = r(point);
    let theta = theta(point);
    (theta.sin() / r, r * theta.cos())
}
