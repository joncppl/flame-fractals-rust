pub mod variations;

use std::vec::Vec;
use rand::prelude::*;
use rand::distributions::WeightedIndex;
use image::{ImageBuffer, Rgb};
use matrix::prelude::Conventional;

pub struct Coeffs {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
    pub e: f32,
    pub f: f32,
}

pub struct Variation {
    pub coeffs: Coeffs,
    pub foo: fn((f32, f32)) -> (f32, f32),
    pub w: f32,
}

pub struct Function {
    pub variations: Vec<Variation>,
    pub probability: u32,
    pub colour: usize,
}

pub struct FlameDescriptor {
    pub functions: Vec<Function>,
    pub n_iterations: u32,
    pub n_samples: u32,
    pub xrange: (f32, f32),
    pub yrange: (f32, f32),

    // for output image. NOT histogram dimensions
    pub width: usize,
    pub height: usize,

    pub super_factor: usize,

    pub gamma: f32,

    pub palette: Vec<(u8, u8, u8)>,
}

struct Histogram {
    histogram: Conventional<u32>,
    colour: ImageBuffer<Rgb<u8>, Vec<u8>>,
}

fn perform_iteration(function: &Function, point: (f32, f32)) -> (f32, f32) {
    let mut new_point = (0.0, 0.0);
    for variation in function.variations.iter() {
        let variation_res = (variation.foo)((
            variation.coeffs.a * point.0
                + variation.coeffs.b * point.1
                + variation.coeffs.c,
            variation.coeffs.d * point.0
                + variation.coeffs.e * point.1
                + variation.coeffs.f));
        new_point = (
            variation.w * variation_res.0 + new_point.0,
            variation.w * variation_res.1 + new_point.1
        )
    }
    new_point
}

fn flame_space_to_histo_space(point: (f32, f32), xrange: (f32, f32), yrange: (f32, f32), hist_size: (usize, usize)) -> (usize, usize) {
    (((point.0 - xrange.0) / (xrange.1 - xrange.0) * (hist_size.0 as f32)) as usize,
     ((point.1 - yrange.0) / (yrange.1 - yrange.0) * (hist_size.1 as f32)) as usize)
}

fn perform_sample_step(descriptor: &FlameDescriptor, histogram: &mut Histogram, choice: &WeightedIndex<u32>) {
    let mut rng = rand::thread_rng();

    let mut point = ( // calculate a random sample starting point
                      rng.gen_range(descriptor.xrange.0, descriptor.xrange.1),
                      rng.gen_range(descriptor.yrange.0, descriptor.yrange.1)
    );

    for iteration in 0..descriptor.n_iterations {
        let function_i = choice.sample(&mut rng);
        point = perform_iteration(&descriptor.functions[function_i], point);
        if iteration > 20 { // plot the point

            let hi = flame_space_to_histo_space(point, descriptor.xrange, descriptor.yrange, (histogram.histogram.columns, histogram.histogram.rows));

            if hi.0 >= histogram.histogram.columns || hi.1 >= histogram.histogram.rows {
                continue; // point fell out of our histogram... we can't use it
            }
            histogram.histogram[hi] += 1;

            let (mut r, mut g, mut b) = descriptor.palette[descriptor.functions[function_i].colour];
            let old_colour = histogram.colour[(hi.0 as u32, hi.1 as u32)];
            r = r / 2 + old_colour[0] / 2;
            g = g / 2 + old_colour[1] / 2;
            b = b / 2 + old_colour[2] / 2;
            histogram.colour[(hi.0 as u32, hi.1 as u32)] = Rgb([r, g, b]);
        }
    }
}

fn reduce_histogram(descriptor: &FlameDescriptor, histo: &Conventional<u32>) -> Conventional<f32> {
    let mut reduced_histogram = Conventional::new((descriptor.width, descriptor.height));

    let den = (descriptor.super_factor * descriptor.super_factor) as f32;

    for x in 0..descriptor.width {
        for y in 0..descriptor.height {
            let mut ha: f32 = 0.0;

            for j in 0..descriptor.super_factor {
                for k in 0..descriptor.super_factor {
                    ha += histo[(x * descriptor.super_factor + j, y * descriptor.super_factor + k)] as f32
                }
            }

            ha /= den;

            reduced_histogram[(x, y)] = ha;
        }
    }

    reduced_histogram
}

fn reduce_image(descriptor: &FlameDescriptor, image: &ImageBuffer<Rgb<u8>, Vec<u8>>) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
    let mut reduced_image = ImageBuffer::new(descriptor.width as u32, descriptor.height as u32);

    let den = (descriptor.super_factor * descriptor.super_factor) as f32;

    for x in 0..descriptor.width {
        for y in 0..descriptor.height {
            let mut c: [f32; 3] = [0.0, 0.0, 0.0];

            for j in 0..descriptor.super_factor {
                for k in 0..descriptor.super_factor {
                    let p = image[((x * descriptor.super_factor + j) as u32, (y * descriptor.super_factor + k) as u32)];
                    c[0] += p[0] as f32;
                    c[1] += p[1] as f32;
                    c[2] += p[2] as f32;
                }
            }

            c[0] /= den;
            c[1] /= den;
            c[2] /= den;

            reduced_image[(x as u32, y as u32)] = Rgb([c[0] as u8, c[1] as u8, c[2] as u8]);
        }
    }

    reduced_image
}

fn histo_largest(histo: &Conventional<f32>) -> f32 {
    let mut largest: f32 = 0.0;
    for x in 0..histo.columns {
        for y in 0..histo.columns {
            if histo[(x, y)] > largest {
                largest = histo[(x, y)];
            }
        }
    }
    largest
}

pub fn perform_flame(descriptor: FlameDescriptor) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
    let buffer_width = descriptor.width * descriptor.super_factor;
    let buffer_height = descriptor.height * descriptor.super_factor;

    let mut hist= Histogram{
        histogram: Conventional::new((buffer_height, buffer_width)),
        colour: ImageBuffer::new(buffer_width as u32, buffer_height as u32)
    };

    let mut probs: Vec<u32> = vec!();
    for function in descriptor.functions.iter() {
        probs.push(function.probability);
    }
    let choice = WeightedIndex::new(&probs).unwrap();
    for _sample in 0..descriptor.n_samples as u32 {
        perform_sample_step(&descriptor, &mut hist, &choice);
    }

    // reduce
    let reduced_histogram = reduce_histogram(&descriptor, &hist.histogram);
    let mut reduced_image = reduce_image(&descriptor, &hist.colour);
    let histo_largest = histo_largest(&reduced_histogram);
    let log_histo_largest = histo_largest.log10();

    let gamma_factor = 1.0 / descriptor.gamma;


    for x in 0..descriptor.width {
        for y in 0..descriptor.height {
            let alpha = reduced_histogram[(x, y)].log10() / log_histo_largest;
            let gca = alpha.powf(gamma_factor);
            let p = reduced_image[(x as u32, y as u32)];
            reduced_image[(x as u32, y as u32)] = Rgb([
                (p[0] as f32 * gca) as u8,
                (p[1] as f32 * gca) as u8,
                (p[2] as f32 * gca) as u8
            ]);
        }
    }

    reduced_image
}