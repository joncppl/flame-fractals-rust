pub mod flame;

use minifb::{WindowOptions, Window};

const WIDTH: usize = 500;
const HEIGHT: usize = 500;

fn main() {
    let mut window = Window::new("Flame",
                                 WIDTH,
                                 HEIGHT,
                                 WindowOptions::default()).unwrap_or_else(|e| {
        panic!("{}", e);
    });

    let mut r_a: f32 = -1.0; // -0.681206
    let mut r_e: f32 = 1.0;

    for _ in 0..1000 {
        let desc = flame::FlameDescriptor {
            n_iterations: 3000,
            n_samples: 50000,
            xrange: (-1.777, 1.777),
            yrange: (-1.0, 1.0),

            width: WIDTH,
            height: HEIGHT,

            super_factor: 4,

            gamma: 4.0,

            palette: vec![
                (255, 0, 0),
                (0, 255, 0),
                (0, 0, 255)
            ],

            functions: vec![
                flame::Function {
                    variations: vec![
                        flame::Variation {
                            coeffs: flame::Coeffs { a: r_a, b: 0.98396, c: 0.43268, d: -0.9542476, e: 0.642503, f: -0.995898 },
                            foo: flame::variations::variation2,
                            w: 1.0,
                        }
                    ],
                    probability: 1,
                    colour: 0,
                },
                flame::Function {
                    variations: vec![
                        flame::Variation {
                            coeffs: flame::Coeffs { a: -0.681206, b: -0.0779465, c: 0.20769, d: 0.255065, e: -0.0416126, f: -0.262334 },
                            foo: flame::variations::variation2,
                            w: 1.0,
                        }
                    ],
                    probability: 1,
                    colour: 1,
                },
                flame::Function {
                    variations: vec![
                        flame::Variation {
                            coeffs: flame::Coeffs { a: -0.681206, b: -0.0779465, c: 0.20769, d: 0.255065, e: r_e, f: -0.262334 },
                            foo: flame::variations::variation6,
                            w: 1.0,
                        }
                    ],
                    probability: 2,
                    colour: 2,
                }
            ],
        };

        r_a += 0.01;
        r_e -= 0.01;

        let rendered_image = flame::perform_flame(desc);

        // hacky display
        let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];
        let mut i = 0;
        for pix in rendered_image.pixels() {
            buffer[i] = (0xFF as u32) << 24 | (pix[0] as u32) << 16 | (pix[1] as u32) << 8 | pix[2] as u32;
            i += 1;
        }
        window.update_with_buffer(&buffer).unwrap();
    }
}
